<?php

return [
    "name" => "language",
    "group" => "extensions",
    "path" => "lar-admin-extensions/language",
    "description" => "Extension of localization.",
    "version" => "1.0.0",
    "icon" => "nut",
    "namespace" => "Lar\\Admin\\Language",
    "class" => "Language",
    "repo" => "https://lar-admin-extensions@bitbucket.org/lar-admin-extensions/language.git",
    "required" => [
        "lar-admin-extensions/config"
    ]
];
