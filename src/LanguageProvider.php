<?php

namespace Lar\Admin\Language;

use Lar\Admin\AdminServiceProvider;
use Lar\Admin\Middleware\Authenticate;

/**
 * LanguageProvider Class
 * 
 * @package Lar\Admin\Language
 */
class LanguageProvider extends AdminServiceProvider
{
    /**
     * Protected variable Commands
     * 
     * @var array
     */
    protected $commands = [
    
    ];

    /**
     * Protected variable Observers
     * 
     * @var array
     */
    protected $observers = [
    
    ];

    /**
     * Protected variable RouteMiddleware
     * 
     * @var array
     */
    protected $routeMiddleware = [
    
    ];

    /**
     * Protected variable MiddlewareGroups
     * 
     * @var array
     */
    protected $middlewareGroups = [
    
    ];

    /**
     * Public method boot
     * 
     * @return void
     */
    public function boot() {
        
        $this->publishes([
            __DIR__ . '/../resources/langs' => resource_path('lang/vendor/language'),
        ]);
    }

    /**
     * Public method register
     * 
     * @return void
     */
    public function register() {

        \Admin::registerExtension('lar-admin-extensions/language');
        $this->registerRouteMiddleware();
        $this->commands($this->commands);
        $this->registerObservers();
        $this->loadMigrationsFrom(__DIR__ . '/../resources/migrations');
        Authenticate::addBootstrap(__DIR__ . '/bootstrap.php');
    }

}
