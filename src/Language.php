<?php

namespace Lar\Admin\Language;

use Lar\Admin\Commands\ExtendInstall;
use Lar\Admin\Commands\ExtendRemove;

/**
 * Language Class
 * 
 * @package Lar\Admin\Language
 */
class Language
{

    /**
     * Static method install
     *
     * @param ExtendInstall $installer
     * @return void
     */
    static function install(ExtendInstall $installer) {

        $installer->info('Extension [Language] installed!');
    }

    /**
     * Static method uninstall
     *
     * @param ExtendRemove $uninstaller
     * @return void
     */
    static function uninstall(ExtendRemove $uninstaller) {

        $uninstaller->info('Extension [Language] run uninstall...');
    }
}
