<?php

namespace Lar\Admin\Language;

use Illuminate\Support\Facades\Facade;

/**
 * LanguageFacade Class
 * 
 * @package Lar\Admin\Language
 */
class LanguageFacade extends Facade
{
    /**
     * Protected Static method getFacadeAccessor
     * 
     * @return void
     */
    protected static function getFacadeAccessor() {
        return Language::class;
    }

}
